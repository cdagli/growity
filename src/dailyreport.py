import webapp2
import urllib2
import logging
import json
import jinja2, os
from google.appengine.api import mail

JINJA_ENVIROMENT = jinja2.Environment(
    loader = jinja2.FileSystemLoader(os.path.dirname(__file__) + "/templates")
    )

def get_API_Data(lat, lng): 
    url = "http://www.benimhavam.com/forecastserver/hourlysimple?lat=" + lat + "&lng=" + lng
    try:
        result = urllib2.urlopen(url)
    except urllib2.URLError, e:
        logging.error('Request error: ' + e);
    content = result.read()
    json_content = json.loads(content)['data']
    return json_content

def send_mail(sender, to, html, subject):
    logging.debug('Start sending email')
    message = mail.EmailMessage()
    message.sender = sender
    message.to = to
    message.html = html
    message.subject = subject
    message.send()

class DailyReport(webapp2.RequestHandler):
    def get(self):
        result = get_API_Data()
        
        JINJA_ENVIROMENT.globals = {'lines': result}
        template = JINJA_ENVIROMENT.get_template('email.html')
        
        send_mail("cdagli@gmail.com", "cdagli@gmail.com", template.render(), "GAE Test Email")
        self.response.status = 200; 
        
                   
app = webapp2.WSGIApplication([
    ('/.*/dailyreport', DailyReport)
], debug=True)
