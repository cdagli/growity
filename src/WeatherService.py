########################################
# Created by canerdagli on 14/07/15.   #
########################################

import webapp2
from google.appengine.ext import ndb
import jinja2
import os
import json
from datetime import datetime

JINJA_ENVIROMENT = jinja2.Environment(
    loader = jinja2.FileSystemLoader(os.path.dirname(__file__) + "/templates")
    )

DEFAULT_DATASTORE_NAME = 'location_datastore'

def location_key(location_name=DEFAULT_DATASTORE_NAME):
    return ndb.Key('Location', location_name)


class LocationPoint(ndb.Model):
    name = ndb.StringProperty(indexed=True)
    latitude = ndb.StringProperty(indexed=False)
    longitude = ndb.StringProperty(indexed=False)
    created = ndb.DateTimeProperty(auto_now_add=True)

class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()

        return json.JSONEncoder.default(self, o)

class MainPage(webapp2.RequestHandler):
    
    #Render template, existing entries loadad from /places route with jQuery
    def get(self):
        JINJA_ENVIROMENT.globals = {'uri_for': webapp2.uri_for}
        template = JINJA_ENVIROMENT.get_template('index.html')
        self.response.out.write(template.render())

    #Create new entry
    def post(self):
        location_name = self.request.get('location_name',
                                          DEFAULT_DATASTORE_NAME)
        newLocation = LocationPoint(parent=location_key(location_name))
        
        newLocation.name = self.request.get("name")
        newLocation.latitude = "{0:.4f}".format(float(self.request.get('lat')))
        newLocation.longitude = "{0:.4f}".format(float(self.request.get('lng')))
        keyReturned = newLocation.put()
        json_string = {'result' : keyReturned.id()}
        self.response.out.write(json.dumps(json_string))
    
    #Update existing entry
    def put(self):
        location_name = self.request.get('location_name',
                                          DEFAULT_DATASTORE_NAME)
        obj = LocationPoint.get_by_id(int(self.request.get('id')), parent=location_key(location_name))
        obj.name = self.request.get("name")
        obj.latitude = "{0:.4f}".format(float(self.request.get('lat')))
        obj.longitude = "{0:.4f}".format(float(self.request.get('lng')))
        obj.put(); 
    
        
class LocationsPage(webapp2.RequestHandler):
    
    #Get existing entries 
    def get(self):
        location_query = LocationPoint.query().order(LocationPoint.name)
        locationPoints = location_query.fetch()
        print locationPoints
        self.response.headers['Content-Type'] = 'application/json'
        json_string = json.dumps([dict(p.to_dict(), **dict(id=p.key.id())) for p in locationPoints], cls=DateTimeEncoder)
        self.response.out.write(json_string)
    
    #Delete entry
    def post(self):
        location_name = self.request.get('location_name',
                                          DEFAULT_DATASTORE_NAME)
        obj = LocationPoint.get_by_id(int(self.request.get('id')), parent=location_key(location_name))
        obj.key.delete();

        
app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/places', LocationsPage),
], debug=True)
