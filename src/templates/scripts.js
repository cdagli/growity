$( document ).ready(function() {
		    console.log( "ready!" );
		    
		    //Hide map and chart divs 
		    $("#chartDiv").hide();
		    $("#mapDiv").hide();
		    
			//Bind button functions  
			$("#btnAdd").bind("click", Add);
			$("#closeChart").bind("click", HideChart);
			$("#closeMap").bind("click", HideMap);
			
			//Get initial records 
			getInitialRecords();
});

function getInitialRecords()
{
    $.getJSON( "/places", function( data ) {
		  $.each( data, function( key, val ) {
		    var new_row = "<tr>" +
		    			  "<td style='display:none;'>" + val.id   + "</td>" +
		    			  "<td>" + val.name   + "</td>" +
		   				  "<td>" + val.latitude   + "</td>" +
		                  "<td>" + val.longitude  + "</td>" + 
		                  "<td><a class='btnEdit'>Edit</a></td>" + 
		                  "<td><a class='btnMap'>Map It</a></td>" +
		                  "<td><a class='btnTemp'>Temperature</a></td>" +
		                  "<td><a class='btnDelete'>Delete</a>" +
		                  "</td></tr>"
			$('#loc_table').append(new_row);
		  });
		  
		  $(".btnEdit").one("click", Edit); 
		  $(".btnTemp").bind("click", Temp);
		  $(".btnMap").bind("click", Map);
		  $(".btnDelete").one("click", Delete); 
  });
}

function initializeMap(lat, lng, name) {
	var myLatlng = new google.maps.LatLng(lat, lng);
    var mapOptions = {
      center: myLatlng,
      zoom: 8
    };
    var map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: name
    });
    contentString = '<div id="content">'+
				    '<div id="siteNotice">'+
				    '</div>'+
				    '<h1 id="firstHeading" class="firstHeading">'+name+'</h1>'+
				    '<div id="bodyContent"><br>'+
				    '<p>Lat: ' + lat + ' Lng: ' + lng + '</p>'+
				    '</div>'+
				    '</div>';
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
      });
  }
function Add(){ 
	console.log("Add pressed!");
	$("#loc_table").prepend( 
			"<tr>"+ 
			"<td style='display:none;'></td>" +
			"<td><input id='txtName' type='text'/></td>"+ 
			"<td><input id='txtLatitude' type='text' maxlength='7'/></td>"+ 
			"<td><input id='txtLongitude' type='text' maxlength='7'/></td>"+ 
			"<td><a class='btnSave'>Save </a></td>"+
			"<td><a class='btnCancel'>Cancel </a></td>"+
			"<td></td><td></td>" +
			"</tr>"); 
	$(".btnSave").one("click", Save);	
	$(".btnCancel").one("click", Cancel); 
}; 

function Save(){ 
	console.log("Save pressed!");
	var par = $(this).parent().parent(); 
	var tdId = par.children("td:nth-child(1)");
	var tdName = par.children("td:nth-child(2)"); 
	var tdLatitude = par.children("td:nth-child(3)"); 
	var tdLongitude = par.children("td:nth-child(4)"); 
	var tdButtonEdit = par.children("td:nth-child(5)");
	var tdButtonMap = par.children("td:nth-child(6)");
	var tdButtonTemp = par.children("td:nth-child(7)");
	var tdButtonDelete = par.children("td:nth-child(8)");
	
	if (Check_Name(tdName.children("input[type=text]").val()) && 
		Check_Lng(tdLongitude.children("input[type=text]").val()) &&
		Check_Lat(tdLatitude.children("input[type=text]").val())) { 

		tdName.html(tdName.children("input[type=text]").val()); 
		tdLatitude.html(tdLatitude.children("input[type=text]").val()); 
		tdLongitude.html(tdLongitude.children("input[type=text]").val()); 
		tdButtonEdit.html("<a class='btnEdit'>Edit </a>");
		tdButtonMap.html("<a class='btnMap'>Map it </a>");
		tdButtonTemp.html("<a class='btnTemp'>Temperature </a>");
		tdButtonDelete.html("<a class='btnDelete'>Delete </a>");
		$(".btnEdit").one("click", Edit); 
		$(".btnDelete").one("click", Delete); 
		$.post('/', { name: tdName.html(), 
					  lat : tdLatitude.html(), 
					  lng : tdLongitude.html()},
					  function(returnedData){
						  console.log(JSON.parse(returnedData).result);
						  tdId.html(JSON.parse(returnedData).result);
			  });  
	}
	$(".btnSave").one("click", Save); 
	$(".btnTemp").unbind().bind("click", Temp);
	$(".btnMap").unbind().bind("click", Map);
}

function Update(){
	console.log("Update executed!");
	var par = $(this).parent().parent(); 
	var tdId = par.children("td:nth-child(1)");
	var tdName = par.children("td:nth-child(2)"); 
	var tdLatitude = par.children("td:nth-child(3)"); 
	var tdLongitude = par.children("td:nth-child(4)"); 
	var tdButtonEdit = par.children("td:nth-child(5)");
	var tdButtonMap = par.children("td:nth-child(6)");
	var tdButtonTemp = par.children("td:nth-child(7)");
	var tdButtonDelete = par.children("td:nth-child(8)");
	
	if (Check_Name(tdName.children("input[type=text]").val()) && 
		Check_Lng(tdLongitude.children("input[type=text]").val()) &&
		Check_Lat(tdLatitude.children("input[type=text]").val())) { 
		tdName.html(tdName.children("input[type=text]").val()); 
		tdLatitude.html(tdLatitude.children("input[type=text]").val()); 
		tdLongitude.html(tdLongitude.children("input[type=text]").val()); 
		tdButtonEdit.html("<a class='btnEdit'>Edit </a>");
		tdButtonMap.html("<a class='btnMap'>Map it </a>");
		tdButtonTemp.html("<a class='btnTemp'>Temperature </a>");
		tdButtonDelete.html("<a class='btnDelete'>Delete </a>");
		$(".btnEdit").one("click", Edit); 
		$(".btnDelete").one("click", Delete);  
		$.ajax({
		    url: '/',
		    type: 'PUT',
		    data: {id: tdId.html(), name: tdName.html(), lat : tdLatitude.html(), lng: tdLongitude.html()},
		    success: function(result) {
		        // Do something with the result
		    }
		});
	}
	$(".btnUpdate").one("click", Update);
	$(".btnTemp").unbind().bind("click", Temp);
	$(".btnMap").unbind().bind("click", Map);
}
	
function Edit(){ 
	console.log("Edit pressed");
	var par = $(this).parent().parent(); 
	var tdName = par.children("td:nth-child(2)"); 
	var tdLatitude = par.children("td:nth-child(3)"); 
	var tdLongitude = par.children("td:nth-child(4)"); 
	var tdButtonEdit = par.children("td:nth-child(5)");
	var tdButtonMap = par.children("td:nth-child(6)");
	var tdButtonTemp = par.children("td:nth-child(7)");
	var tdButtonDelete = par.children("td:nth-child(8)");
	tdName.html("<input type='text' id='txtName' value='"+tdName.html()+"'/>"); 
	tdLatitude.html("<input type='text' id='txtLatitude' value='"+tdLatitude.html()+"' maxlength='7'/>"); 
	tdLongitude.html("<input type='text' id='txtLongitude' value='"+tdLongitude.html()+"' maxlength='7'/>"); 
	tdButtonEdit.html("<a class='btnUpdate' class='btnUpdate'>Update</a>"); 
	tdButtonMap.html("<a></a>");
	tdButtonTemp.html("<a></a>");
	tdButtonDelete.html("<a></a>");
	$(".btnUpdate").one("click", Update); 
}

function Cancel(){
	console.log("Cancel pressed!");
	var par = $(this).parent().parent(); //tr 
	var tdId = par.children("td:nth-child(1)");
	console.log(tdId.html());
    par.remove();
}

function Delete(){ 
	console.log("Delete pressed!");
	var par = $(this).parent().parent(); //tr 
	var tdId = par.children("td:nth-child(1)");
	console.log(tdId.html());
    par.remove();
	$.ajax({
	    url: '/places',
	    type: 'POST',
	    data: {id: tdId.html()},
	    success: function(result) {
	        // Do something with the result
	    }
	});
} 

function Temp()
{
	console.log("Temp pressed");
	
	var par = $(this).parent().parent(); 
	var tdLatitude = par.children("td:nth-child(3)"); 
	var tdLongitude = par.children("td:nth-child(4)"); 
    $.getJSON('http://www.benimhavam.com/forecastserver/hourlysimple', 
    	     {
    	        lat: tdLatitude.html(),
    	        lng: tdLongitude.html()
    	     }, 
    	     function(data) {
    	    	 var value = data.data;
    	    	 var tempData = [];
    	         $.each( value, function( key, val ) {
    	        	 tempData.push(val.temperature);
    	         });
    	         var flags = [], uniqueDays = [], l = value.length, i;
    	         for( i=0; i<l; i++) {
    	             if( flags[value[i].day]) 
    	            	 uniqueDays.push("");
    	             else { 
    	            	 flags[value[i].day] = true;
    	            	 uniqueDays.push(value[i].day);
    	             }
    	         }
    	         $("#chartDiv").slideDown("slow");
    	         drawLineChart(tempData, uniqueDays);
    	         
    	     }
    );
}

function Map(){
	console.log("Map button pressed");
	var par = $(this).parent().parent(); //tr 
	var tdName = par.children("td:nth-child(2)"); 
	var tdLatitude = par.children("td:nth-child(3)"); 
	var tdLongitude = par.children("td:nth-child(4)");
	$("#mapDiv").slideDown("slow");
	initializeMap(tdLatitude.html(), tdLongitude.html(), tdName.html());
}

function HideChart(){
	$("#chartDiv").slideUp("slow");
}

function HideMap(){
	$("#mapDiv").slideUp("slow");
}

function Check_Lat(mValue){
	if (!$.isNumeric(mValue) || mValue > 90 || mValue < 0 ){
		alert("Latitude should be between 0 & 90");
		return false;
	}
	return true;
}

function Check_Lng(mValue){
	if (!$.isNumeric(mValue) || mValue > 180 || mValue < -180 ){
		alert("Longitude should be between -180 & 180");
		return false;
	}
	return true;
}

function Check_Name(mValue){
	if (mValue.length == 0 ){
		alert("Name can not be empty!");
		return false;
	}
	else if(mValue.length > 25){
		alert("Name is too long! (Max = 25 chars)");
		return false;
	}
	return true;
}

function drawLineChart(mData, mLabels){
    var data = {
   		    labels: mLabels,
   		    datasets: [
   		        {
   		            label: "Temperature",
   		            fillColor: "#F9C5BE",
   		            strokeColor: "#E8513B",
   		            pointColor: "rgba(220,220,220,1)",
   		            pointStrokeColor: "#fff",
   		            pointHighlightFill: "#fff",
   		            pointHighlightStroke: "rgba(220,220,220,1)",
   		            data: mData
   		        }
   		    ]
   		};
   
    var options = {
   		    ///Boolean - Whether grid lines are shown across the chart
   		    scaleShowGridLines : true,
   		    //String - Colour of the grid lines
   		    scaleGridLineColor : "rgba(0,0,0,.05)",
   		    //Number - Width of the grid lines
   		    scaleGridLineWidth : 1,
   		    //Boolean - Whether to show horizontal lines (except X axis)
   		    scaleShowHorizontalLines: true,
   		    //Boolean - Whether to show vertical lines (except Y axis)
   		    scaleShowVerticalLines: true,
   		    //Boolean - Whether the line is curved between points
   		    bezierCurve : true,
   		    //Number - Tension of the bezier curve between points
   		    bezierCurveTension : 0.4,
   		    //Boolean - Whether to show a dot for each point
   		    pointDot : false,
   		    //Number - Radius of each point dot in pixels
   		    pointDotRadius : 4,
   		    //Number - Pixel width of point dot stroke
   		    pointDotStrokeWidth : 1,
   		    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
   		    pointHitDetectionRadius : 0,
   		    //Boolean - Whether to show a stroke for datasets
   		    datasetStroke : true,
   		    //Number - Pixel width of dataset stroke
   		    datasetStrokeWidth : 2,
   		    //Boolean - Whether to fill the dataset with a colour
   		    datasetFill : true,

   		};
    var ctx = $("#myChart").get(0).getContext("2d");
	var myLineChart = new Chart(ctx).Line(data, options);	
}



